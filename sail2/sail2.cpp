// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014


#include "sail2.h"


std::vector<std::string> split(std::string, char);
// void ws_open(websocketIO* ws);
// void ws_initialize(websocketIO*, boost::property_tree::ptree);
// void ws_requestNextFrame(websocketIO*, boost::property_tree::ptree);
// void ws_stopMediaCapture(websocketIO*, boost::property_tree::ptree);
// void ws_setItemPositionAndSize(websocketIO*, boost::property_tree::ptree);
// void ws_finishedResize(websocketIO*, boost::property_tree::ptree);
void ws_open(void*);
void ws_initialize(void*, boost::property_tree::ptree);
void ws_requestNextFrame(void*, boost::property_tree::ptree);
void ws_stopMediaCapture(void*, boost::property_tree::ptree);
void ws_setItemPositionAndSize(void*, boost::property_tree::ptree);
void ws_finishedResize(void*, boost::property_tree::ptree);

bool continuous_resize = true;

sail::sail(const char *appname, int ww, int hh, enum sagePixFmt pixelfmt, std::string url, float frate)
{
	ws_uri    = url;
	width     = ww;
	height    = hh;
	format    = pixelfmt;
	frameRate = frate;
	applicationName = std::string(appname);

	frame = (unsigned char*)malloc(getBufferSize());
	memset(frame, 0, getBufferSize());

	wsio = new websocketIO();
	wsio->set_object(this);
	wsio->openCallback(ws_open);

	wsio->on("initialize",       ws_initialize);
	wsio->on("requestNextFrame", ws_requestNextFrame);
	wsio->on("stopMediaCapture", ws_stopMediaCapture);
	wsio->on("finishedResize",   ws_finishedResize);
	wsio->on("setItemPositionAndSize", ws_setItemPositionAndSize);

	wsio->run(ws_uri);
}

sail::~sail()
{
	// Close the socket
	wsio->close();
}

int sail::getBufferSize()
{
	if (format == PIXFMT_888)
		return (width*height*3);
	else if (format == PIXFMT_YUV)
		return (width*height*2);
	else return 0;
}

static int _initialized = 0;

sail* createSAIL(const char *appname, int ww, int hh,
					enum sagePixFmt pixelfmt, const char *url, float frate)
{
	sail *sageInf;

	// Initialize a few things
	if (! _initialized) {
		initTime();
		_initialized = 1;
	}

	// Allocate the sail object
	sageInf = new sail(appname, ww, hh, pixelfmt, url, frate);
	if (sageInf == NULL)
		return NULL;

	return sageInf;
}


void deleteSAIL(sail *sageInf)
{
	delete sageInf;
	exit(0);
}


std::string nextBuffer(sail *sageInf)
{
	unsigned char *ptr = (unsigned char*) sageInf->getBuffer();
	std::string jpegFrame = convertToJpeg(ptr, sageInf->getWidth(), sageInf->getHeight());

    size_t out;
    char* image_base64 = base64_encode((const unsigned char*)jpegFrame.c_str(), jpegFrame.length(), &out);

    std::string string_base64(image_base64, image_base64 + out);
    free(image_base64);

    return string_base64;
}

void swapWithBuffer(sail *sageInf, unsigned char *pixptr)
{
	// Swap the main sail object
	unsigned char *ptr = (unsigned char*) sageInf->getBuffer();
	memcpy(ptr, pixptr, sageInf->getBufferSize());
	// sageInf->swapBuffer();
}

void swapBuffer(sail *sageInf)
{
	// Swap buffer for the main object
	// sageInf->swapBuffer();
}


void processMessages(sail *sageInf)
{
	// Is the socket closed
	if (sageInf->wsio->is_done()) {
		exit(0);
	}

	// more to add
}





/******** Helper Functions ********/
std::vector<std::string> split(std::string s, char delim) {
	std::stringstream ss(s);
	std::string item;
	std::vector<std::string> elems;
	while (std::getline(ss, item, delim)) {
		elems.push_back(item);
	}
	return elems;
}

/******** WebSocket Callback Functions ********/
void ws_open(void *ptr) {
	sail* sageInf   = (sail*)ptr;
	websocketIO* ws = sageInf->wsio;

	printf("WEBSOCKET OPEN\n");

    // send addClient message
	boost::property_tree::ptree data;
	data.put<std::string>("clientType", sageInf->applicationName);
	data.put<bool>("sendsMediaStreamFrames", true);
	data.put<bool>("receivesWindowModification", true);
	data.put<bool>("receivesInputEvents", true);

	ws->emit("addClient", data);
}

void ws_initialize(void *ptr, boost::property_tree::ptree data) {
	sail* sageInf   = (sail*)ptr;
	websocketIO* ws = sageInf->wsio;

	sageInf->uniqueID = data.get<std::string> ("UID");
	printf("ID: %s\n", sageInf->uniqueID.c_str());

	if (!sageInf->uniqueID.empty()) {
		printf("Initializing connection\n");

		boost::property_tree::ptree emit_data;
		emit_data.put<std::string>("id",    sageInf->uniqueID + "|0");
		emit_data.put<std::string>("title", sageInf->applicationName);
		// black pixel 1x1 GIF
		emit_data.put<std::string>("src",      "R0lGODlhAQABAIAAAAAAAAAAACH5BAAAAAAALAAAAAABAAEAAAICTAEAOw==");
		emit_data.put<std::string>("type",     "image/gif");
		emit_data.put<std::string>("encoding", "base64");
		emit_data.put<int>("width",  sageInf->getWidth());
		emit_data.put<int>("height", sageInf->getHeight());
		ws->emit("startNewMediaStream", emit_data);
	}
}

void ws_requestNextFrame(void *ptr, boost::property_tree::ptree data) {
	static double lastframetime = getTime();
	 
	sail* sageInf   = (sail*)ptr;
	websocketIO* ws = sageInf->wsio;

	boost::property_tree::ptree state;

	state.put<std::string>("src",      nextBuffer(sageInf));
	state.put<std::string>("type",     "image/jpeg");
	state.put<std::string>("encoding", "base64");

	boost::property_tree::ptree emit_data;
	emit_data.put<std::string>("id", sageInf->uniqueID + "|0");
	emit_data.put_child("state", state);
	ws->emit("updateMediaStreamFrame", emit_data);

	double now = getTime();
    double instantfps = 1.0/(now-lastframetime);
    fprintf(stderr, "FPS: %.2f\r", instantfps);
    lastframetime = now;
}

void ws_stopMediaCapture(void *ptr, boost::property_tree::ptree data) {
	sail* sageInf   = (sail*)ptr;
	websocketIO* ws = sageInf->wsio;

	std::string streamId = data.get<std::string> ("streamId");
	int idx = atoi(streamId.c_str());
	printf("STOP MEDIA CAPTURE: %d\n", idx);

	deleteSAIL(sageInf);
}

void ws_setItemPositionAndSize(void *ptr, boost::property_tree::ptree data) {
 	sail* sageInf   = (sail*)ptr;
	websocketIO* ws = sageInf->wsio;

   // update browser window size during resize
	if (continuous_resize) {
		std::string id = data.get<std::string> ("elemId");
		std::vector<std::string> elemData = split(id, '|');
		std::string uid = "";
		int idx = -1;
		if (elemData.size() == 2){
			uid = elemData[0];
			idx = atoi(elemData[1].c_str());
		}

		if (uid == sageInf->uniqueID) {
			std::string w = data.get<std::string> ("elemWidth");
			std::string h = data.get<std::string> ("elemHeight");
			std::string x = data.get<std::string> ("elemLeft");
			std::string y = data.get<std::string> ("elemTop");
			float neww = atof(w.c_str());
			float newh = atof(h.c_str());
			float newx = atof(x.c_str());
			float newy = atof(y.c_str());
			printf("New pos & size: %.0f x %.0f - %.0f x %.0f\n", newx, newy, neww, newh);
		}
	}
}

void ws_finishedResize(void *ptr, boost::property_tree::ptree data) {
	sail* sageInf   = (sail*)ptr;
	websocketIO* ws = sageInf->wsio;

	std::string id = data.get<std::string> ("id");
	std::vector<std::string> elemData = split(id, '|');
	std::string uid = "";
	int idx = -1;
	if(elemData.size() == 2){
		uid = elemData[0];
		idx = atoi(elemData[1].c_str());
	}

	if(uid == sageInf->uniqueID) {
		std::string w = data.get<std::string> ("elemWidth");
		std::string h = data.get<std::string> ("elemHeight");

		float neww = atof(w.c_str());
		float newh = atof(h.c_str());

		printf("New size: %.0f x %.0f\n", neww, newh);
	}
}
