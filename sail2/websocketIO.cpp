// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

//
//  websocketIO.cpp
//
//  Created by Thomas Marrinan on 4/3/14.
//
//

#include "websocketIO.h"

websocketIO::websocketIO() : m_open(false),m_done(false),obj(NULL) {
	m_client.clear_access_channels(websocketpp::log::alevel::all);
	m_client.set_access_channels(websocketpp::log::alevel::app);
	m_client.init_asio();

    // Bind the handlers we are using
	using websocketpp::lib::placeholders::_1;
	using websocketpp::lib::placeholders::_2;
	using websocketpp::lib::bind;

    // Register our handlers
	m_client.set_socket_init_handler(bind(&websocketIO::on_socket_init, this, ::_1));
	m_client.set_tls_init_handler(bind(&websocketIO::on_tls_init, this, ::_1));
	m_client.set_message_handler(bind(&websocketIO::on_message, this, ::_1, ::_2));
	m_client.set_open_handler(bind(&websocketIO::on_open, this, ::_1));
	m_client.set_close_handler(bind(&websocketIO::on_close, this, ::_1));
	m_client.set_fail_handler(bind(&websocketIO::on_fail, this, ::_1));
}

bool websocketIO::is_done() {
	return m_done;
}

void websocketIO::set_object(void *p) {
	// store a pointer to an opaque pointer, users' data
	obj = p;
}

void websocketIO::on_socket_init(websocketpp::connection_hdl hdl) {
    // init socket
}

websocketIO::context_ptr websocketIO::on_tls_init(websocketpp::connection_hdl hdl) {
	context_ptr ctx(new boost::asio::ssl::context(boost::asio::ssl::context::tlsv1));

	try {
		ctx->set_options(boost::asio::ssl::context::default_workarounds |
			boost::asio::ssl::context::no_sslv2 |
			boost::asio::ssl::context::single_dh_use);
	} catch (std::exception& e) {
		printf("ERRRRRRR\n");
		std::cout << e.what() << std::endl;
	}
	return ctx;
}

void websocketIO::on_open(websocketpp::connection_hdl hdl) {
	m_client.get_alog().write(websocketpp::log::alevel::app, "Connection opened");

	scoped_lock guard(m_lock);
	m_open = true;

	if (obj)
		m_openCallback(obj);
	else
		m_openCallback(this);
}

void websocketIO::on_close(websocketpp::connection_hdl hdl) {
	m_client.get_alog().write(websocketpp::log::alevel::app, "Connection closed");

	scoped_lock guard(m_lock);
	m_done = true;
}

void websocketIO::close() {
	m_client.get_alog().write(websocketpp::log::alevel::app, "Closing");
	scoped_lock guard(m_lock);
	m_done = true;
    m_client.close(m_hdl, websocketpp::close::status::normal, "done");
}

void websocketIO::on_fail(websocketpp::connection_hdl hdl) {
	m_client.get_alog().write(websocketpp::log::alevel::app, "Connection failed");

	scoped_lock guard(m_lock);
	m_done = true;
}

void websocketIO::on_message(websocketpp::connection_hdl hdl, message_ptr msg) {
	boost::property_tree::ptree json;
	std::istringstream iss(msg->get_payload());
	boost::property_tree::read_json(iss, json);

	std::string func = json.get<std::string>("f");
	boost::property_tree::ptree data = json.get_child("d");

	if(messages.find(func) != messages.end()){
		if (obj)
			messages[func](obj, data);
		else
			messages[func](this, data);
	}
}

void websocketIO::send_message(std::string msg) {
	if (!m_open) {
		return;
	}

	websocketpp::lib::error_code ec;
	m_client.send(m_hdl, msg, websocketpp::frame::opcode::text, ec);

	if (ec) {
		m_client.get_alog().write(websocketpp::log::alevel::app, "Send Error: "+ec.message());
	}
}

void websocketIO::openCallback(void (*callback)(void*)) {
	m_openCallback = callback;
}

void websocketIO::on(std::string name, void (*callback)(void*, boost::property_tree::ptree)) {
	messages[name] = callback;
}

void websocketIO::emit(std::string name, boost::property_tree::ptree data) {
	boost::property_tree::ptree root;
	root.put<std::string>("f", name);
	root.put_child("d", data);

	std::ostringstream oss;
	boost::property_tree::write_json(oss, root, false);

	send_message(oss.str());
}

void websocketIO::run(const std::string & uri) {
	websocketpp::lib::error_code ec;
	client::connection_ptr con = m_client.get_connection(uri, ec);

	if (ec) {
		m_client.get_alog().write(websocketpp::log::alevel::app, ec.message());
	}

	m_hdl = con->get_handle();
	m_client.connect(con);

	websocketpp::lib::thread asio_thread(&client::run, &m_client);
}

