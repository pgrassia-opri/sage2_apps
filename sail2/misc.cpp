// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

#include <sys/time.h>

struct timeval tv_start;

void initTime()
{
    gettimeofday(&tv_start, 0);
}

double getTime()
{
    struct timeval tv;
    gettimeofday(&tv, 0);
    return (double)(tv.tv_sec - tv_start.tv_sec) + (double)(tv.tv_usec - tv_start.tv_usec) / 1000000.0 ;
}
