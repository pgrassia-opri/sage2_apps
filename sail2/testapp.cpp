// SAGE2 is available for use under the SAGE2 Software License
//
// University of Illinois at Chicago's Electronic Visualization Laboratory (EVL)
// and University of Hawai'i at Manoa's Laboratory for Advanced Visualization and
// Applications (LAVA)
//
// See full text, terms and conditions in the LICENSE.txt included file
//
// Copyright (c) 2014

#include "sail2.h"

//
// usage:  ./testapp <URI>
//         ./testapp wss://localhost:9292
//         ./testapp wss://traoumad.evl.uic.edu
//

int
main(int argc, char **argv)
{
	sail *sageInf;

	// Read some uncompressed RGB data
	unsigned char *data = (unsigned char*)malloc(1920*1080*3);
	memset(data, 0, 1920*1080*3);
	FILE *f = fopen("pattern.rgb", "r");
    fread(data, 1, 1920*1080*3, f);
	fclose(f);

	// Create SAIL2 object
	sageInf = new sail("testapp", 1920, 1080, PIXFMT_888, argv[1], 30);

	double lastframetime = getTime();

	int count = 0;
	bool done = false;
	while (! done) {
		count++;
		if (count>=100000) {
			done = true;
		}

		swapWithBuffer(sageInf, data);

		processMessages(sageInf);

		usleep(1000000/30);
		double now = getTime();
	    double instantfps = 1.0/(now-lastframetime);
	    //fprintf(stderr, "FPS: %.2f\r", instantfps);
	    lastframetime = now;
	}

	delete sageInf;

	return 0;
}