## SAGE2 API for external apps: sail2

C++ API for SAGE2

### First time
	* make setup
	* to install websocketpp library (using git)

### make
	* builds library libsail.so
	* builds testapp

### run
	* ./testapp URI
	* ./testapp wss://localhost:9292

