// SAGE2 is available for use under the following license, commonly known
//          as the 3-clause (or "modified") BSD license:
//
// Copyright (c) 2014, Electronic Visualization Laboratory,
//                     University of Illinois at Chicago
// All rights reserved.
//
// http://opensource.org/licenses/BSD-3-Clause
// See included LICENSE.txt file


var rfb  = require('rfb2');
var jpeg = require('node-jpeg');

function vncClient(host, port, pwd, loader) {
	this.host = host;
	this.port = port;
	this.pwd  = pwd;
	this.conn = null; 
	this.stack  = null;
	this.pixels = null;
	this.frame  = 0;
};

vncClient.prototype.pointerEvent = function(x,y,b) {
	this.conn.pointerEvent(x,y,b);
}
vncClient.prototype.keyEvent = function(keySym, isDown) {
	this.conn.keyEvent(keySym, isDown);
}


vncClient.prototype.open = function(opencb,framecb ) {
	console.log("opening: ", this.host, this.port);
	this.conn = rfb.createConnection({
		host: this.host,
		port: this.port,
		password: this.pwd
	});

	this.conn.on('error', function(e) {
		console.log("error", e);
	});

	var self = this;
	var conn = this.conn;
	
	this.conn.on('connect', function() {
		conn.requestUpdate(false, 0, 0, conn.width, conn.height);
		console.log('successfully connected and authorised');
		console.log('remote screen name: ' + conn.title + ' width:' + conn.width + ' height: ' + conn.height);
		conn.autoUpdate = true;
		self.stack = new jpeg.FixedJpegStack(conn.width, conn.height, 'bgra');
		self.stack.setQuality(65);
		self.pixels = new Buffer(conn.width*conn.height*4);
		if (opencb) opencb();
	});

	// screen updates
	this.conn.on('rect', function(rect) {
		//console.log("Got a screen update");

		switch(rect.encoding) {
			case rfb.encodings.raw:
		    //console.log("Raw", rect.x, rect.y, rect.width, rect.height);

		    // go to encode
		    self.stack.push(rect.data, rect.x, rect.y, rect.width, rect.height);
		    // put the pixels in the store: row per row
		    for (var j = 0; j < rect.height; j++) {
		      // buf.copy(targetBuffer, [targetStart], [sourceStart], [sourceEnd])
		      rect.buffer.copy(self.pixels, 4*(rect.x+(rect.y+j) * conn.width), 4*(j*rect.width), 4*((j+1)*rect.width));
		  }
		  break;

		  case rfb.encodings.copyRect:
		    //console.log("Copy", rect.src.x, rect.src.y, rect.width, rect.height, "to", rect.x, rect.y);
		    // put the pixels in the store: row per row
		    for (var j = 0; j < rect.height; j++) {
				// buf.copy(targetBuffer, [targetStart], [sourceStart], [sourceEnd])
				self.pixels.copy(self.pixels, 4*(rect.x+(rect.y+j)*conn.width), 4*(rect.src.x+(rect.src.y+j)*conn.width), 4*(rect.src.x+rect.width+(rect.src.y+j)*conn.width));
		  	}
		    // send back the whole image to be compressed
		    self.stack.push(self.pixels, 0,0, conn.width, conn.height);
		    break;

		    case rfb.encodings.hextile:
		    console.log("hextile", rect.x, rect.y, rect.width, rect.height);
		    // not fully implemented
		    //rect.on('tile', handleHextileTile); // emitted for each subtile
		    break;
		}
	});

	this.conn.on('frame', function() {
		//console.log("new frame");
		self.stack.encode(function (image, error) {
			self.frame = self.frame + 1;
			if (framecb) framecb(image);
		});
	});
};



module.exports = vncClient;
