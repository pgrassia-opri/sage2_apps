# README #

Repository for SAGE2 external applications (streaming applications, written in C/C++, python, javascript, ...).

## VNC client: vnc_client

A Javascript VNC client with interaction passthrough

## sage2Streaming: decklinkcapture

Streaming of decklink video cards output to a SAGE2 server.

## Low-level USB pointer: sage2_usb

USB "driver" from SAGE2 pointer

## CEF-based browser application for SAGE2: cefsimple

Streaming browser using Chromium Embedded Framework

## SAGE2 API for external apps: sail2

C++ API for SAGE2

